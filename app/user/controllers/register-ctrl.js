"use strict";
angular
  .module("user")
  .controller("RegisterCtrl", function(
    $scope,
    $user,
    $message,
    $state,
    $ionicHistory,
    $ionicLoading,
    $timeout,
    _logo,
    $stateParams
  ) {
    $scope.logo = _logo;

    $scope.mail = [$stateParams.email_user];

    $scope.loginWithFacebook = function() {
      $ionicLoading.show();
      $user
        .facebookLogin()
        .then(function() {
          $timeout(function() {
            $ionicLoading.hide();
            $state.go("profile");
          }, 200);
        })
        .catch(function() {
          $ionicLoading.hide();
          $message.alert("We had problems accessing your account, try again");
        });
    };

    $scope.registrar = function(
      name,
      last_name,
      company,
      job_position,
      email,
      password,
      phone_number
    ) {
      if (!name) {
        $message.alert("Please, enter your name");
      } else if (!last_name) {
        $message.alert("Please, enter your last name");
      } else if (!company) {
        $message.alert("Please, enter your company");
      } else if (!job_position) {
        $message.alert("Please, enter your job position");
      } else if (!email) {
        $message.alert("Please, Enter your email");
      } else if (!password) {
        $message.alert("Please, enter your password");
      } else if (password.split("").length < 6) {
        $message.alert("The password must to contain six characters");
      }else if (!phone_number){
        console.log(phone_number)
        $message.alert("El numero ingresado debe tener almenos 10 digitos");
      } else {
        $ionicLoading.show();
        $user
          .register(name, last_name , company , job_position , email , password)
          .then(function() {
            $user.login(email, password).then(function() {
              $state.go("global.landing")
              .then(function() {
                $ionicHistory.nextViewOptions({ disableBack: true });
                $ionicLoading.hide();
              });
            });
          })
          .catch(function(error) {
            $ionicLoading.hide();
            if (error.code == "auth/email-already-in-use") {
              $message.alert("This email is already in use");
            } else if (error.code == "auth/weak-password") {
              $message.alert("La contraseña es muy débil");
            }
          });
      }
    };

    $scope.goHome = function() {
      $ionicHistory.clearHistory();
      $ionicHistory.clearCache();
      $state.go("global.landing");
    };
  });
