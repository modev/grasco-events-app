"use strict";
// mcht@mintic.gov.co Applemintic20
angular
  .module("push", [])
  .service("$push", function( $state, $http, $mocifire, $message, _firebase,$ionicSideMenuDelegate,$log,$filter,$commons,$rootScope) {
    var service = this;
    
       
  /* FUNCIONAMIENTO NOTIFICACIONES
Cuando la notificaciones llegan del servidor despues, de estar escuchando si alguna no notificacion esta
pendiende en el campo status, en el path /guardaNotificaciones
 de firebase, llegan al app y se guardan en pushtoken
con el id del dispositivo para depues ser mostradas en la bandeja de notificaciones.

*/
console.log('hola desde el servicio--0-0-0- ')
service.uuid = localStorage.getItem('deviceId');
service.props = {notificaciones:{"aasd":"dfasdf"},badges:{},test:{test:"123"}};
service.removeNotification = function(noti){
  _firebase.database().ref('pushtoken/'+service.uuid+'/notifications').child(noti.$key).set(null, function(error) {});
}



  // _firebase.database().ref('pushtoken/'+service.uuid+'/notifications').limitToLast(15).on('value',function(snap){
  //   var temporal = setInterval(function(){
  //     // console.log('hola')
  //     service.props.notificaciones = $filter('orderBy')($filter('toArray')(snap.val(), true), 'date', true);
  //     var notificacionesLeidas = $filter("orderBy")(
  //        $filter("toArray")(snap.val(), true),
  //        "date",
  //        true
  //      ).filter(function(notificacion){ return !notificacion.readed});
  //     service.props.badges = notificacionesLeidas.length;
  //   },10)
  //   setTimeout(function(){
  //     clearInterval(temporal);
  //   },10000)
    
    
  //    $commons.apply($rootScope);
  //   })    




    var push = null;
    var auth = _firebase.auth();
    auth.onAuthStateChanged(function (user) {
      if (!user) return;
      service.uid = user.uid;
  });

    service.init = function() {

      if (!window.cordova) return;

      if (window.device.platform == "browser"){ return}

      push = PushNotification.init({
        android: {
          senderID: '187347411048',
          forceShow: true,
          clearBadge: true
        },
        ios: { alert: true, badge: true, sound: true, clearBadge: true }
      });

      push.on("registration", function(data) {
        push.token = data.registrationId;
        var idusuario = service.uid || null;
        console.log('====================>>>>>>>>>>> ', idusuario)
        service.register(push.token,service.uid);
       
         //guardamos el id del dispositivo y el token en el localstorage
      localStorage.setItem('token', push.token); 
      localStorage.setItem('deviceId', device.uuid);

 
      console.log('el token de ios-->>', push.token);
      });

      push.on("notification", function(data) {
        // $ionicSideMenuDelegate.toggleRight()    
        /*$message.alert('You have a new(s) notification(s)',10000,'bottom',function(){
          // $ionicSideMenuDelegate.toggleRight()    
            
        });*/
        $message.alert('You have a new notification',10000,'bottom',function(){
          // $ionicSideMenuDelegate.toggleRight()    
            
        });
    
        console.log('esta es la data de la notificacion----',service.uid,'ooooo',data)
            //verifica que la varible esta definida para no tener errore en navegador tanto como en dispositivo
            console.log(device.uuid);
            if(typeof(device) !== "undefined"){
            var uuid = (device && device.uuid) ? device.uuid : null;
            uuid = uuid || token || now;
          }else{
            var uuid = uuid || token || now;
          }  
          


        // service.guardaNotificaciones(data,uuid)
        if (
          ionic.Platform.platform() == "ios" &&
          data.additionalData.foreground &&
          data.additionalData.payload.web
        ) {
          return $message
            .confirm(
              data.additionalData.payload.titulo,
              data.additionalData.payload.mensaje,
              "Look",
              "Cancel"
            )
            .then(function() {
              cordova.InAppBrowser.open(
                data.additionalData.payload.webpage,
                "_system"
              );
              if (service.uid)
                $mocifire.set(
                  [
                    "users",
                    service.uid,
                    "beacons",
                    "_clicks",
                    data.additionalData.payload.tipo
                  ],
                  true
                );
            });
        } else if (data.additionalData.foreground) {
          return;
        }

        if (data.additionalData.payload && data.additionalData.payload.state) {
          if (data.additionalData.payload.params)
            $state.go(
              data.additionalData.payload.state,
              data.additionalData.payload.params
            );
          else $state.go(data.additionalData.payload.state);
        } else if (
          data.additionalData.payload &&
          data.additionalData.payload.web
        ) {
          cordova.InAppBrowser.open(
            data.additionalData.payload.webpage,
            "_system"
          );
          if (service.uid)
            $mocifire.set(
              [
                "users",
                service.uid,
                "beacons",
                "_clicks",
                data.additionalData.payload.tipo
              ],
              true
            );
        }
      });
    };

    service.register = function(token, uid) {
 
    
      if (push && push.token) {
        var uuid = device.uuid || push.token;
        if (service.uid) {
          //para usuarios logueados
          $mocifire.update(["pushtoken",uuid], {
            type: ionic.Platform.platform(),
            token: push.token,
            date: Date.now(),
            uid : service.uid,
            'log': 'yes'
          });
          
          $mocifire.update(["users", uid, "push"], {
            type: ionic.Platform.platform(),
            token: push.token
          });

        } else {
          //para usuarios anonimos
          var yaexistetoken = false;
          

          $mocifire.update(["pushtoken",uuid], {
            type: ionic.Platform.platform(),
            token: push.token,
            date: Date.now(),
            'log': 'no'
          });
        }
      }
    };

    function string2int(string) {
      var num = 0;
      for (var i = 0; i < string.length; i++) {
        num += string.charCodeAt(i);
      }
      return num;
    }

    // $push.reminder(id, message, date);
    service.reminder = function(id, message, date) {
      if (!window.cordova) return;

      //restamos 30 minutos
      date.setMinutes(date.getMinutes() - 30);

      cordova.plugins.notification.local.schedule({
        id: string2int(id),
        title: "",
        text: message + ", will start soon",
        at: date
      });
    };

    service.cancelReminder = function(id) {
      if (!window.cordova) return;

      cordova.plugins.notification.local.cancel(string2int(id));
    };

    /**
     * $push.send('chat', 49);
     * @param {type} type 'chat' - 'request' - 'answer'
     * @param {type} name Name
     * @param {type} id_receiver Identification of user reciever.
     */
    service.send = function(type, name, id_receiver, idUser,message) {
      console.log('******************************************ll', type, name, id_receiver)
      var messageWrapper = {
        "status":"pending",
        "author":name,
        "title": name + " Te escribio: ",
        "message":message || '',
        "predefinedMessage":type,
        "date": new Date().getTime(),
        "recipienttype":"users",
        "recipients":'user',
        "sent":{total:0,ios:0,android:0},
        "state": 'main.networking.chatDetail',
        "section": "Chat",
        "pushToUser" : id_receiver
      };
      if(idUser){
        messageWrapper["params"] = {chatID : idUser};
      }

      $mocifire.push(["notifications"], messageWrapper);
  
    };





 


  
  });
              


