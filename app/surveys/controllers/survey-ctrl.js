"use strict";
angular
  .module("surveys")
  .controller("SurveyCtrl", function(
    $scope,
    $mocifire,
    $state,
    $message,
    $ionicSlideBoxDelegate,
    $ionicHistory,
    $user,
    event_id,
    $commons,
    $sce
  ) {
    var ctrl = this;
$scope.valido = false;
    $scope.$on("$ionicView.enter", function() {});

    $scope.response = {};
    $scope.no_polls_message = "There are no  polls open.";
    $scope.survey = null;

    $mocifire
      .get(["events", event_id, "enabled_survey"])
      .then(function(survey_id) {
        $scope.survey = survey_id;
        $mocifire.get(["surveys", "types"]).then(function(types) {
          $scope.types = types;
          $mocifire
            .get(["surveys", "polls", event_id, survey_id, "questions"])
            .then(function(questions) {
              $scope.lengthQuestions = questions
                ? Object.keys(questions).length - 1
                : 0;

              //to render question as HTML
              angular.forEach(questions, function(value, index) {
                questions[index].ask = $sce.trustAsHtml(questions[index].ask);
              });

              $scope.questions = questions;

              $ionicSlideBoxDelegate.update();
            });
        });
      });

    $scope.$on("$ionicSlides.sliderInitialized", function(event, data) {
      $scope.slider = data.slider;
    });

    $scope.element = function(key, res) {
      if ($scope.response[key].indexOf(res) > -1)
        $scope.response[key].splice($scope.response[key].indexOf(res), 1);
      else $scope.response[key].push(res);
    };

    ctrl.saveResponse = function(survey_id, question_id, user_id, res) {
      var respuestas = res;
      if (res && res.constructor !== Array) {
        respuestas = [res];
      }

      angular.forEach(respuestas, function(value, index) {
        $mocifire.push(["surveys_responses", survey_id, question_id], {
          survey_id: survey_id,
          question_id: question_id,
          response_id: value,
          index: index,
          user_id: user_id,
          event_id: event_id,
          date: Date.now()
        });
      });
    };

    $scope.save = function() {
        $scope.validar();
      angular.forEach($scope.response, function(res, id) {
        ctrl.saveResponse($scope.survey, id, $user.uid, res);

        switch ($scope.types[$scope.questions[id].type]) {
          case "unica":
          case "abierta":
            $mocifire.transaction(
              ["surveys", "polls", event_id, $scope.survey, "results", id, res],
              function(votes) {
                return votes + 1;
              }
            );
            break;

          case "multiple":
            angular.forEach(res, function(key) {
              $mocifire.transaction(
                [
                  "surveys",
                  "polls",
                  event_id,
                  $scope.survey,
                  "results",
                  id,
                  key
                ],
                function(votes) {
                  return votes + 1;
                }
              );
            });
            break;

          case "orden":
            angular.forEach(res, function(key, position) {
              $mocifire.transaction(
                [
                  "surveys",
                  "polls",
                  event_id,
                  $scope.survey,
                  "results",
                  id,
                  key,
                  position
                ],
                function(votes) {
                  return votes + 1;
                }
              );
            });
            break;
        }
      });

      $message.alert("Your answers have been sent successfully");
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
      ctrl.goHome();
    };

    ctrl.goHome = function() {
      $state.go("main.home", { event_id: event_id });
    };

    $scope.validar= function(){
        var con = 0;
        angular.forEach($scope.response,function(key){
            if(key) con++;
            if(con == $scope.lengthQuestions+1) $scope.valido = true;
            console.log('el formulario es valido?',$scope.valido)
        })
    }
  });
