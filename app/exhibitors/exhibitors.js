'use strict';
angular.module('exhibitors', [
  'ionic',
  'ngCordova',
  'ui.router',
  // TODO: load other modules selected during generation
])
.config(function ($stateProvider) {

  // ROUTING with ui.router
  $stateProvider
    // this state is placed in the <ion-nav-view> in the index.html
    .state('main.exhibitors', {
      url: '/exhibitors',
      views: {
        'eventView': {
          templateUrl: 'exhibitors/templates/exhibitors.html',
          controller: 'exhibitorsCtrl as ctrl'
        }
      }
    })

    .state('main.exhibitor', {
      url: '/exhibitors/:idExhibitor',
      views: {
        'eventView': {
          templateUrl: 'exhibitors/templates/exhibitor.html',
          controller: 'ExhibitorCtrl as ctrl'
        }
      }
    });
});
