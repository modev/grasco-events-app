"use strict";
angular
  .module("staticPage", [
    "ionic",
    "ngCordova",
    "ui.router"
    // TODO: load other modules selected during generation
  ])
  .config(function($stateProvider) {
    // ROUTING with ui.router
    $stateProvider
      .state("main.staticPage", {
        url: "/staticPage/{pageId}",
        views: {
          'eventView': {
            templateUrl: "staticPage/templates/staticPage.html",
            controller: "StaticPageCtrl as ctrl"
          }
        }
      });
  });
