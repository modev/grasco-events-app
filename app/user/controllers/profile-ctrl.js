"use strict";
angular
  .module("user")
  .controller("ProfileCtrl", function(
    $scope,
    $user,
    $message,
    $state,
    $mocifire,
    $stateParams,
    $log,
    $ionicLoading,
    $timeout,
    $q,
    $filter,
    $camera,
    $profileimage,
    event_id
  ) {
    var ctrl = this;
    $ionicLoading.show();

    var hideloader = function() {
      $timeout($ionicLoading.hide, 500);
    };

    $scope.tpl = {
      form: "user/templates/profile-form.html"
    };

    $scope.hideButton = {
      val: $stateParams.hide
    };

    $scope.cities = ["Bogotá", "Medellin", "Bucaramanga", "Baranquilla"];

    $scope.form = {};
    $scope.image = {};

    /**
     * Camara
     */
    $scope.changeImage = function(library) {
      $camera.get(library).then(function(blop) {
        $scope.image.change = true;
        $scope.image.blop = blop;
        $scope.image.url = URL.createObjectURL(blop);
      });
    };

    /**
     * init promesa
     */
    var promesas = {};

    promesas.dos = $mocifire
      .get(["users", $user.uid, "info"])
      .then(function(info) {
        if (info) {
          if (info.birthday) info.birthday = new Date(info.birthday);
          $scope.form = info;
          $scope.image.url = info.image || null;
        }
      });
    $mocifire.get("adverts/ciudades").then(function(data) {
      $scope.cities = data;
    });

    /**
     * push
     */
    /*  $scope.push_form = {};
    var push_status = firebase.database().ref('users').child($user.uid).child('push').child('disabled');
    push_status.once('value', function (dato) {
        $scope.push_form.value = dato.val();
    });
*/
    /**
     * resolver promesa
     */
    $q
      .all(promesas)
      .then(function() {
        hideloader();
      })
      .catch(function(err) {
        $log.log("error", err);
        hideloader();
      });

    /**
     * Acciones
     */
    $scope.save = function(form) {
      if ($scope.image.change && $scope.image.blop)
        $profileimage.update($scope.image.blop);

      if (
        (!form.name,
        !form.last_name,
        !form.job_position,
        !form.email,
        !form.company,
        !form.phone_number)
      )
        return $message.alert("You must fill all the fields");
      $user
        .saveProfile({
          name: form.name ? form.name : "",
          last_name: form.last_name ? form.last_name : "",
          job_position: form.job_position ? form.job_position : "",
          email: form.email ? form.email : "",
          phone_number : form.phone_number ? form.phone_number : '',
          company: form.company ? form.company : ""
        })
        .then(function() {
          $message.alert("Your information has been saved successfully");
          alert('tu informacion ha sido guardada')
          $state.go("main.home")
          if (!$scope.hideButton.val) $state.go("global.landing");
        })
        .catch(function() {
          $message.alert("We had problems saving your data, try again.");
        });
    };

    $scope.save_push_status = function() {
      push_status.set($scope.push_form.value);
    };

    $scope.later = function() {
      $state.go("global.landing");
    };
    ctrl.go = function(state, params) {
      console.log('Noo?');
      if (!params) params = {};
      if (event_id){
        params["event_id"] = event_id;
      }
      if (state == "main.favourites" && ctrl.user.isAnonymous) {
        $message.alert(
          "In order to use this functionality you should login first"
        );
        $state.go("login");
      } else {
        $ionicHistory.nextViewOptions({
          disableBack: true
        });
       
        if (state =="global.landing" && event_id){
          $state.go("main.home",params);
        }else{
          $state.go(state,params);
        }
      }
    };
  });
