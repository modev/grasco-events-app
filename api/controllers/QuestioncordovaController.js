/**
 * QuestioncordovaController
 *
 * @description :: Server-side logic for managing questioncordovas
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	


  /**
   * `QuestioncordovaController.platform()`
   */
  platform: function (req, res) {
    return res.json({
      todo: 'platform() is not implemented yet!'
    });
  },


  /**
   * `QuestioncordovaController.rm()`
   */
  rm: function (req, res) {
    return res.json({
      todo: 'rm() is not implemented yet!'
    });
  },


  /**
   * `QuestioncordovaController.ios()`
   */
  ios: function (req, res) {
    return res.json({
      todo: 'ios() is not implemented yet!'
    });
  }
};

