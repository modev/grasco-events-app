'use strict';

angular.module('main')
  .service('$currentView', function ($log,configSettings) {

  var serv = this;

  serv.currentView = 'null';

  /**
   * problema que soluciona: permite modificar la clases de forma dinamica desde el archivo de configuración
  */
    
    //this method set the state of de current view
    serv.setView= function(stateView){
        // localStorage.setItem('dat','col')
        serv.currentView = stateView;
    }
      //this method get the state of de current view
    serv.getView= function(){
        return serv.currentView;
    }

    //comparamos el stado que coincida para obtener la clase a aplicar
    serv.getClass = function(){
        switch(serv.currentView){
            case configSettings.header.customClassHeader.agenda.agenda.state:
                return configSettings.header.customClassHeader.agenda.agenda.class;
            break; 
            case configSettings.header.customClassHeader.agenda.session.state:
                return configSettings.header.customClassHeader.agenda.session.class;
            break; 
            case configSettings.header.customClassHeader.tracks.tracks.state:
                return configSettings.header.customClassHeader.tracks.tracks.class;
            break; 
            case configSettings.header.customClassHeader.speakers.list.state:
                return configSettings.header.customClassHeader.speakers.list.class;
            break; 
            default: return 'none'
            // case configSettings.header.customClassHeader.agenda.agenda.state:
            //     return configSettings.header.customClassHeader.agenda.agenda.class;
            // break;

        }
    }

});
