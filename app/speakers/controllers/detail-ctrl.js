"use strict";
angular
  .module("speakers")
  .controller("SpeakerCtrl", function(
    $scope,
    $state,
    $mocifire,
    $CurrentSession,
    $stateParams,
    $fav,
    $commons,
    $speaker,
    event_id,
    $ionicLoading,
    $user,
    $q
  ) {
    var ctrl = this;

    ctrl.user = $user;
    ctrl.random_speakers = [];
    $ionicLoading.show();

    $mocifire.get(["speakers", $stateParams.idSpeaker]).then(function(data) {
      $ionicLoading.hide();
      ctrl.speaker = data;
      ctrl.fav = $fav.is("speakers", $stateParams.idSpeaker);

      if (ctrl.speaker.info.track) {
        $mocifire.get(["tracks", ctrl.speaker.info.track]).then(function(data) {
          ctrl.track = data;
        });
      }

      $speaker
        .get(ctrl.speaker.info.track, $stateParams.idSpeaker, event_id)
        .then(function(result) {
          ctrl.related = result.speakers;
          ctrl.sessions = result.sessions;
          console.log("sessiones", ctrl.sessions);
          $commons.apply($scope);
        });
    });

    $mocifire.get(["scenarios", event_id]).then(function(data) {
      ctrl.scenarios = data;
    });

    ctrl.irASession = function(session) {
      var scenario =
        ctrl.scenarios && ctrl.scenarios[session.info.place]
          ? ctrl.scenarios[session.info.place].name
          : null;
      var type =
        ctrl.sessiontypes && ctrl.sessiontypes[session.info.type]
          ? ctrl.sessiontypes[session.info.type].name
          : null;
      var track =
        ctrl.tracks && ctrl.tracks[session.info.track]
          ? ctrl.tracks[session.info.track].name
          : null;

      // pasar datos de sesion seleccionada para usarlos en el detalle
      $CurrentSession.set(session, scenario, type, track);
      $state.go("main.session", { sessionid: session.$key });
    };
    /**
     * Favoritos
     */
    ctrl.favorito = function(bool) {
      if (bool) {
        $fav.new("speakers", $stateParams.idSpeaker);
        ctrl.fav = true;
      } else {
        $fav.remove("speakers", $stateParams.idSpeaker);
        ctrl.fav = false;
      }
      if (
        $scope.$root &&
        $scope.$root.$$phase != "$apply" &&
        $scope.$root.$$phase != "$digest"
      )
        $scope.$apply();
    };

    /**
     * Speaker Aleatorio
     */
    var randomSpeakers = function() {
      //Cargamos todos los id de speakers del evento
      $mocifire.get(["event_speakers", event_id]).then(function(speakers_ids) {
        //borramos el speaker actual para que no se repita
        delete speakers_ids[$stateParams.idSpeaker];

        var speakers_keys = Object.keys(speakers_ids);
        var random_speakers_ids = [];

        var cuantos = 2;
        var i = 0;
        while (i++ <= cuantos && speakers_keys.length > 0) {
          var randon_position = Math.floor(
            Math.random() * speakers_keys.length
          );
          var random_key = speakers_keys[randon_position];

          random_speakers_ids.push(random_key);
          speakers_keys.splice(randon_position, 1);
        }


        var promesas = {};
        angular.forEach(random_speakers_ids, function(speaker_id, position) {
          promesas[speaker_id] = $mocifire.get(["speakers", speaker_id]);
        });

        $q.all(promesas).then(function(speakers_data) {
          ctrl.random_speakers = speakers_data;

        console.log("array", speakers_data);
        });
      });
    };
    randomSpeakers();
  });
