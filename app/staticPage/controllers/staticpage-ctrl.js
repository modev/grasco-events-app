"use strict";
angular
  .module("staticPage")
  .controller("StaticPageCtrl", function(
    $ionicHistory,
    $log,
    $mocifire,
    $scope,
    $state,
    $stateParams,
    event_id
  ) {
    /* Falta hacer esto más dinamico */
    var ctrl = this;
    ctrl.pageId = $stateParams.pageId;
    $mocifire.get("pages/" + ctrl.pageId).then(function(data) {
      $scope.item = data;
    });

    $scope.goBack = function() {
      $state.go("main.home", { event_id: event_id });
    };

    ctrl.go = function(state, params) {
      console.log('fjhf');
      if (!params) params = {};
      params["event_id"] = event_id;
      
      if (state == "main.favourites" && ctrl.user.isAnonymous) {
        $message.alert(
          "In order to use this functionality you should login first"
        );
        $state.go("login");
      } else {
        $ionicHistory.nextViewOptions({
          disableBack: true
        });

        $state.go(state, { event_id: event_id });
      }
    };
  });
