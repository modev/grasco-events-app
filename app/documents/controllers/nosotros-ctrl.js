"use strict";
angular
  .module("nosotros")
  .controller("nosotrosCtrl", function(
    $ionicLoading,
    ShareFile,
    $log,
    $mocifire,
    $scope,
    $state,
    $stateParams,
    event_id,
    $commons
  ) {
    var ctrl = this;
    $ionicLoading.show();
    ctrl.ShareFile = ShareFile;

    //titulo dinamico
    $scope.title = "DOCUMENTS";
    console.log($stateParams );
    $mocifire.database
      .ref("document_category").on("value",function(snap){
        $scope.categories = snap.val();
        if ($stateParams.idCategory){
         
          var cat = $scope.categories[$stateParams.idCategory];
          if (cat){
            $scope.title = cat.name;
            $commons.apply($scope);
          }
        }
    });


    var $query = $mocifire.database
      .ref("documents");

    if ($stateParams.idCategory){
      $query = $query.orderByChild("event_category")
      .equalTo(event_id+"_"+$stateParams.idCategory);
    }else{
      $query = $query.orderByChild("event")
      .equalTo(event_id);
    }
    $query.on("value", function(snapshot) {
        $scope.content = snapshot.val();
        $ionicLoading.hide();
      });
      
    $scope.goBack = function() {
      $state.go("main.home", { event_id: event_id });
    };
  });
