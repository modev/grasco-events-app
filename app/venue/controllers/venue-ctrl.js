'use strict';
angular.module('venue').controller('VenueCtrl', function ($mocifire, $scope, $ionicScrollDelegate, $sce, event_id, $comoLlegar) {
  
  var ctrl = this;

  ctrl.comoLlegar = $comoLlegar;
  ctrl.showImage = false;

// Show map

  $scope.$watch(function() { return eventService.getID()
  }, function(newVal){
    $mocifire.get(['venues']).then(function (data) {
      var id = Object.keys(data);
      ctrl.loadMap(id[0]);
    });

      // console.log('Associates changes into: ', newVal)
  }, true);
  


  // Init map
  $scope.defaults = {
    tileLayer: "http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
    zoomControl: false
  };

  $scope.markers = {};
  $scope.center = {};

  
  //Mostrar ubicacion en el mapa
  ctrl.active = null;

  ctrl.setMap = function (mapobj, id) {
    ctrl.active = id;
    $scope.center = mapobj;

    $scope.markers.main = {
      lat: mapobj.lat,
      lng: mapobj.lng
    }
  };

  ctrl.checkActive = function (id) {
    return ctrl.active === id;
  };

  // Mostrar imagen
  ctrl.images = {};

  ctrl.setImg = function (id) {
    ctrl.images[id] = !ctrl.images[id];
    $ionicScrollDelegate.resize();
  };

  ctrl.checkImg = function (id) {
    return ctrl.images[id];
  };

  ctrl.toogleImage = function (){

    !ctrl.showImage ? ctrl.showImage = true : ctrl.showImage = false;

  }
    /**
    * get venues
    */
  ctrl.venues = {};
  
  $mocifire.get(['venues', event_id]).then(function (data) {

    if (!data) return;

    ctrl.venues = data;

    console.log(data);

    angular.forEach(ctrl.venues, function (val, key) {
      val.id = key;
      val.description = $sce.trustAsHtml(val.description) || null;
    });
    
    var _mapinit = ctrl.venues[Object.keys(ctrl.venues)[0]];
    ctrl.setMap(_mapinit.map, _mapinit.id);
  });

});
