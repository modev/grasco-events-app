"use strict";
angular.module("main").directive("adverts", function() {
  return {
    restrict: "E",
    scope: {
      eventid: "@"
    },
    templateUrl: "main/templates/banner.html",
    controller: function($scope, $mocifire, $filter, $timeout) {
      var currentPlatform = ionic.Platform.platform();

      /**
       * init slider
       */
      $scope.slider = null;
      $scope.sliderOptions = {
        loop: true,
        effect: "slide",
        autoplay: 10000,
        autoplayDisableOnInteraction: false,
        pagination: false
      };

      $mocifire.get("adverts/"+$scope.eventid).then(function(data) {
        $scope.adverts = $filter("toArray")(data, true);

        //Entre mas elementos alla, el tiempo que duran expuestos va a ser menor
        var multiplicador_autoplay = 1;
        angular.forEach($scope.adverts, function(val) {
          multiplicador_autoplay++;
        });
        if (multiplicador_autoplay)
          $scope.sliderOptions = $scope.sliderOptions / multiplicador_autoplay;

        $timeout(function() {
          if ($scope.slider) {
            $scope.slider.stopAutoplay();
            $scope.slider.update();
            $scope.slider.startAutoplay();
          }
        }, 200);
      });

      /**
       * Abrir link externo
       */
      $scope.link = function(path) {
        if (!path) return;
        cordova.InAppBrowser.open(path, "_system", "location=yes");
      };

      $scope.redirectPage = function(urlItem) {
        urlItem.type = urlItem.type || "external";
        if (urlItem.type == "external") {
          cordova.InAppBrowser.open(urlItem.links.url, "_system", "location=yes");
        } else {
          $state.go(urlItem.url,urlItem.params);
        }
      };
    }
  };
});