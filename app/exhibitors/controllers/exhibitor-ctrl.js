'use strict';
angular.module('exhibitors')
  .controller('ExhibitorCtrl', function ($log, $mocifire, $stateParams, event_id) {

  var ctrl = this;
  ctrl.exchange = {};
  ctrl.documents = [];

  ctrl.idExchange = $stateParams.idExhibitor;
  $mocifire.get(['exhibitors', event_id, $stateParams.idExhibitor]).then(function (data) {
    data.description  =  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque arcu turpis, blandit consectetur eleifend vitae, condimentum eu tellus. Sed convallis hendrerit lectus vitae maximus. Fusce ultricies, lectus in viverra maximus, erat diam porttitor urna, vel suscipit ligula mauris nec sem. Sed ornare lacinia eros vel ultrices. Curabitur in vulputate dolor. Nunc metus neque, facilisis sed lobortis at, interdum vel ex. Proin dignissim lacinia nibh vitae placerat. Curabitur consectetur ipsum a ligula scelerisque interdum. Nullam pulvinar blandit maximus. Sed tortor nisi, sodales quis nibh at, fringilla vestibulum risus.";
    ctrl.exchange = data;
  });

  ctrl.openUrl = function (path) {
    window.open(path, '_blank', 'location=no');
  };

});
