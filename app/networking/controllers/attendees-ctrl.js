"use strict";
angular
  .module("networking")
  .controller("AttendeesCtrl", function(
    $scope,
    $log,
    $mocifire,
    $state,
    $ionicHistory,
    $ionicLoading,
    $Contact,
    $q,
    $timeout,
    $user,
    $http,
    $message,
    event_id,
    $commons
  ) {

    $scope.$on('$ionicView.beforeEnter', function (event, viewData) { viewData.enableBack = true; });
  
    var ctrl = this;
    $scope.search = {};
    ctrl.user = $user.uid;
    ctrl.fbuserid = ctrl.user;

    if (!$user.uid) {
      $ionicLoading.hide();
      $message.alert(
        "In order to use this functionality you should login first"
      );
      $state.go("login");
    }

    /* para bloquear que los usuarios no registrados vean el listado */
    $scope.$on("$ionicView.enter", function() {
      firebase
        .database()
        .ref("users/" + $user.uid + "/events/" + event_id)
        .on("value", function(snap) {
          var registrado = snap.val();
          ctrl.notregistered = (!registrado)?true:null;
          $commons.apply($scope);
        });
    });

    /**
     * Attendees
     * @type {{cache: Array, data: Array}}
     */
    ctrl.attendees = {
      cache: [],
      data: []
    };

    ctrl.cargarUsuarios = function() {
      /**
       * traer usuarios del evento actual
       */
      var usersRef = firebase
        .database()
        .ref("users")
        .orderByChild("events/" + event_id)
        .startAt(true);

      usersRef.on("child_added", function(user) {
        // Get id
        var user_id = user.getKey();
        // Revisar si el usuario es el actual
        if (user_id === $user.uid) return;
        // Three way data binding
        $Contact.getUser(user_id).then(function(on_user) {
          on_user.id = user_id;
          ctrl.attendees.data.push(on_user);
        });
      });
    };

    /**
     * Enviar solicitud de contacto (business card exchange)
     */
    ctrl.request = function(receiverID, check, index) {
      // retornar si esta en proceso de aprobacion
      if (check === false) return;

      $Contact
        .request(receiverID)
        .then(function() {
          $log.log("request success");
          $message.alert("Request sent.");
        })
        .catch(function(err) {
          $log.log("request error:", err);
        });
    };

    /* para bloquear que los usuarios no registrados vean el listado */

    firebase
      .database()
      .ref("users/" + $user.uid + "/events/" + event_id)
      .on("value", function(snap) {
        var registrado = snap.val();
        if (!registrado) {
          ctrl.notregistered = true;
        } else {
          ctrl.notregistered = null;
          ctrl.cargarUsuarios();
        }
      });
  });
