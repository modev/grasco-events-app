'use strict';
angular.module('favourites').service('$fav', function ($user, $mocifire, $rootScope) {

    var $fav = this;

    $fav.list = {};

    $fav.init = function () {
        $mocifire.on(['users', $user.uid, 'favorites'], function (favorites) {
            
            if (!favorites) return;

            if (favorites.programme) {
                $fav.list.programme = {};
                angular.forEach(favorites.programme, function (item, key) {
                    $mocifire.get(['programme', item.event_id, key]).then(function (data) {
                        if (!data) return;
                        data.event_id = item.event_id;
                        $fav.list.programme[key] = data;
                    });
                });
            }
            if (favorites.speakers) {
                $fav.list.speakers = {};
                angular.forEach(favorites.speakers, function (item, key) {
                    $mocifire.get(['speakers', key]).then(function (data) {
                        if (data)
                        $fav.list.speakers[key] = data;
                    });
                });
            }

            if (favorites.events) {
                $fav.list.events = {};
                angular.forEach(favorites.events, function (item, key) {
                    $mocifire.get(['events', key, 'info']).then(function (data) {
                        if (!data) return;
                        data.order = new Date(data.dateFrom);
                        $fav.list.events[key] = data;
                    });
                });
            }
           
            if ($rootScope.$root && $rootScope.$root.$$phase != '$apply' && $rootScope.$root.$$phase != '$digest')
                $rootScope.$apply();

        });
    };

    /**
     * Permite saber si un tipo de contenido se encuentra como favorito.
     * category ->
     */
    $fav.is = function (category, id,quien) {
        return ($fav.list[category] && $fav.list[category][id]);
    };

    $fav.new = function (category, id, event_id) { 
        if (!$fav.list[category]) $fav.list[category] = {};

       
        var favorito = {value: true, event_id: event_id || null};
        //$fav.list[category][id] =favorito;
        // cuando se agrega un favorito como anonimo, luego se va a favoritos
        // y se loguea, se rompe la aplicación porque la primera
        // vez que se agrega los favoritos solo se agrega el event_id mas no la informacion del evento
        // con fav.init se forza a que se agreguela info.         
        $mocifire.get([category, event_id, id]).then(function (data) {
            if (!data) return;
            data.event_id = event_id;
            $fav.list[category][id] = data;

        }).catch(function(e){
            $fav.list[category][id] =favorito;
        });
        // cuando se agrega un favorito como anonimo, luego se va a favoritos
        // y se loguea, se rompe la aplicación porque la primera
        // vez que se agrega los favoritos solo se agrega el event_id mas no la informacion del evento
        // con fav.init se forza a que se agreguela info. 
        //persistimos el favorito
        return $mocifire.set(['users', $user.uid, 'favorites', category, id], 
        {value: true, event_id: event_id || null}).then(function(){


        })
        
        ;
    };

    $fav.remove = function (category, id) {
        if ($fav.list[category])
        $fav.list[category][id] =false;

        return $mocifire.remove(['users', $user.uid, 'favorites', category, id]);
    }

   

});
