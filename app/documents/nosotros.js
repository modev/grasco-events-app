"use strict";
angular
  .module("nosotros", [
    "ionic",
    "ngCordova",
    "ui.router"
    // TODO: load other modules selected during generation
  ])
  .config(function($stateProvider) {
    //States information about Moveconcerts
    // ROUTING with ui.router
    $stateProvider
      // this state is placed in the <ion-nav-view> in the index.html
      .state("main.documents", {
        url: "/documents",
        views: {
          eventView: {
            templateUrl: "documents/templates/documentos.html",
            controller: "nosotrosCtrl as ctrl"
          }
        }
      })
      .state("main.documentsbycategory", {
        url: "/documentsbycategory/{idCategory}",
        views: {
          eventView: {
            templateUrl: "documents/templates/documentos.html",
            controller: "nosotrosCtrl as ctrl"
          }
        }
      });
  });
