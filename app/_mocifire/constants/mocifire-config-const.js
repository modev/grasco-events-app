'use strict';
angular.module('_Mocifire').constant("_firebase", firebase.initializeApp({
    apiKey: "AIzaSyDyjJ7OAYs_l33I5wolpbeh4LQEpMn5_DM",
    authDomain: "grasco-app-4d393.firebaseapp.com",
    databaseURL: "https://grasco-app-4d393.firebaseio.com",
    projectId: "grasco-app-4d393",
    storageBucket: "grasco-app-4d393.appspot.com",
    messagingSenderId: "711946618150",
}));

/**
    GRASCO BD

    apiKey: "AIzaSyDyjJ7OAYs_l33I5wolpbeh4LQEpMn5_DM",
    authDomain: "grasco-app-4d393.firebaseapp.com",
    databaseURL: "https://grasco-app-4d393.firebaseio.com",
    projectId: "grasco-app-4d393",
    storageBucket: "grasco-app-4d393.appspot.com",
    messagingSenderId: "711946618150",

    WOF BD 

    apiKey: "AIzaSyB86BwNsFGods9_m5t06yaVp37CWI4yJa0",
    authDomain: "wofapp-1d4e2.firebaseapp.com",
    databaseURL: "https://wofapp-1d4e2.firebaseio.com",
    projectId: "wofapp-1d4e2",
    storageBucket: "wofapp-1d4e2.appspot.com",
    messagingSenderId: "187347411048"

    PMI BD

    apiKey: "AIzaSyDSuPlOn_RUuFwT30QA6-q1f8WITS_qIP0",
    authDomain: "pmi-app-76b95.firebaseapp.com",
    databaseURL: "https://pmi-app-76b95.firebaseio.com",
    projectId: "pmi-app-76b95",
    storageBucket: "pmi-app-76b95.appspot.com",
    messagingSenderId: "140635047834"

 */ 