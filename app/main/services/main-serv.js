"use strict";
angular
  .module("main")
  .service("Main", function($log, $mocifire, $q, $filter, $ionicLoading) {
    var serv = this;

    // some initial data
    serv.menu = {
      data: [],
      cache: {}
    };

    serv.menuHome = {
      data: [],
      cache: {}
    };

    function setGoogle(ua) {
      if (ua && window.ga) {
        window.ga.startTrackerWithId(ua);
        window.ga.trackView("init");
      }
    }

    /**
     * traer settings del evento
     * @returns {*}
     */
    serv.initSettings = function() {
      return $q(function(resolve, reject) {
        $mocifire
          .get(["settings"])
          .then(function(data) {
            if (data) {
              serv.UA = data.analytics || null;
              serv.register = data.register || null;
              setGoogle(serv.UA);
            }
            resolve();
          })
          .catch(function(err) {
            reject(err);
          });
      });
    };

    serv.getMenu = function(event_id) {
        console.log("mocilog",event_id);
      return $q(function(resolve, reject) {
        var cache_key = (event_id)?event_id:"default";

        if (serv.menu.cache[cache_key]) 
        return resolve(serv.menu.cache[cache_key]);

        $ionicLoading.show();
        var menu_query = null;
        if (event_id)
        menu_query = $mocifire.get("events/"+event_id+"/menu");
        else
        menu_query = $mocifire.get("menu");

        menu_query.then(function(data) {
            var menu = $filter("orderBy")(
              $filter("toArray")(data),
              "order"
            );
            serv.menu.cache[cache_key] = menu;
            $ionicLoading.hide();
            resolve(serv.menu.cache[cache_key]);
            console.log("menu", serv.menu.cache[cache_key]);
          })
          .catch(function(err) {
            $ionicLoading.hide();
            reject(err);
          });
      });
    };

    /**Get the menu of home */
    serv.getMenuHome = function(event_id) {
        console.log("mocilog",event_id);
      return $q(function(resolve, reject) {
        var cache_key = (event_id)?event_id:"default";

        if (serv.menuHome.cache[cache_key]) 
        return resolve(serv.menuHome.cache[cache_key]);

        $ionicLoading.show();
        var menuHome_query = null;
        if (event_id)
        menuHome_query = $mocifire.get("events/"+event_id+"/menu_home");
        else
        menuHome_query = $mocifire.get("menu_home");

        menuHome_query.then(function(data) {
            var menuHome = $filter("orderBy")(
              $filter("toArray")(data),
              "order"
            );
            serv.menuHome.cache[cache_key] = menuHome;
            $ionicLoading.hide();
            resolve(serv.menuHome.cache[cache_key]);
            console.log("menu_home", serv.menuHome.cache[cache_key]);
          })
          .catch(function(err) {
            $ionicLoading.hide();
            reject(err);
          });
      });
    };
  });
