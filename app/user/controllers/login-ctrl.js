"use strict";
angular
  .module("user")
  .controller("LoginCtrl", function(
    $scope,
    $user,
    $message,
    $state,
    _logo,
    Main,
    $ionicLoading,
    $ionicHistory
  ) {
    $scope._register = Main.register;
    $scope.logo = _logo;

    $scope.loginWithFacebook = function() {
      $ionicLoading.show();
      $user
        .facebookLogin()
        .then(function() {
          $ionicHistory.nextViewOptions({ disableBack: true });
          $state.go("profile").then($ionicLoading.hide);
        })
        .catch(function() {
          $ionicLoading.hide();
          $message.alert("We had problems accessing your account, try again");
        });
    };

    $scope.anonimo = function() {
      $ionicLoading.show();
      firebase
        .auth()
        .signInAnonymously()
        .then(function() {
          $state.go("global.landing").then(function() {
            $ionicHistory.nextViewOptions({ disableBack: true });
            $ionicLoading.hide();
          });
        })
        .catch(function(error) {
          // Handle Errors here.
          $ionicLoading.hide();
          var errorCode = error.code;
          var errorMessage = error.message;
          // ...
        });
    };

    $scope.goHome = function() {
      var user = sessionStorage.getItem('user')
      console.log('yeah', user);
      $ionicHistory.clearHistory();
      $ionicHistory.clearCache();
      $state.go("global.landing");
    };

    $scope.login = function(email, password) {
      password = "mocion";
      if (!email) return $message.alert("Please enter a valid email");

      /* $ionicLoading.show(); */
      $user
        .login(email, password)
        .then(function() {
          $state.go("global.landing").then(function() {
            console.log('entro')
            sessionStorage.setItem('user', true);
            console.log('user login' , true)
            $ionicHistory.nextViewOptions({ disableBack: true });
            $ionicLoading.hide();
          });
        })
        .catch(function(error) {
          console.log('no existe');
          $ionicLoading.hide();

          if (error.code == "auth/invalid-email") {
            $message.alert(
              "Please enter a valid email, this e-mail is not supported"
            );
            return;
          }

          //sino tiene cuenta lo llevamos a la de registro.
          if (error.code == "auth/user-not-found") {
            $state.go("register", { email_user: email });
            return;
          }

          $message.alert(
            "An error has happened, please try again " + error.code
          );
        });
    };

    $scope.forgotPassword = function() {
      $message
        .prompt(
          "Recuperar password",
          "Por favor ingresa tu e-mail y te enviaremos un correo para restablecer los datos de usuario",
          "Aceptar",
          "Cancelar"
        )
        .then(function(email) {
          if (!/[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/gim.test(email))
            return $message.alert("El correo electrónico es inválido");

          $user
            .forgotPassword(email)
            .then(function() {
              $message.popup(
                "Email sent",
                "We have sent an email to your email with instructions, so you can reset your password"
              );
            })
            .catch(function() {
              $message.alert(
                "The email is invalid or doesn't exist, check it and try again."
              );
            });
        });
    };

    $scope.changePassword = function(password) {
      $user
        .changePassword(password)
        .then(function() {
          console.info("OK");
        })
        .catch(function(error) {
          if (error.code == "auth/weak-password") {
            $message.alert(
              "he password is very weak, it must have at least 6 characters"
            );
          }
        });
    };
  });
