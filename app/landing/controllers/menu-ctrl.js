'use strict';
angular.module('landing').controller('MenuGlobalCtrl',
function ($state, $mocifire, $message, $user, $log,  $ionicHistory,menu, $scope ,_firebase, $filter, $ionicSideMenuDelegate, $commons) {

  var c = console.log;
  var ctrl = this;
  ctrl.user = $user;
  ctrl.menu = menu;
  ctrl.uuid = localStorage.getItem('deviceId')

  ctrl.logout = function () {
    sessionStorage.removeItem('user');
    console.log('la eliminio');
    console.log('deslogueado');
    ctrl.user.logout().then(function () {
        $ionicHistory.nextViewOptions({
            disableBack: true
        });
        $state.go('login');
    })
  };

  // Notifications
  _firebase.database().ref('pushtoken/'+ctrl.uuid+'/notifications').limitToLast(15).on('value',function(snap){
      // console.log('hola')
      if(!snap.val()) {
        ctrl.badges = 0;
        ctrl.notificaciones = '';
        $commons.apply($scope);
        return

      }
      ctrl.notificaciones = $filter('orderBy')($filter('toArray')(snap.val(), true), 'date', true);
      
      var notificacionesLeidas = $filter("orderBy")(
         $filter("toArray")(snap.val(), true),
         "date",
         true
       ).filter(function(notificacion){ return !notificacion.readed});
       ctrl.badges = (Object.keys(notificacionesLeidas).length== 0)? 0: notificacionesLeidas.length;
    
      // console.log('================>>>>>>>>>>>>> ',ctrl.notificaciones);
     $commons.apply($scope);
    })
  //IR AL CHAT
  /*
  ctrl.goChat = function () {
    $state.go('chats');
  };
*/
/**
 * navegar a otro estado
 * @param state
 * @param disableBack
 */

  //ir a la ventana de chats
  $scope.footerGo = function (state) {
      if(ctrl.user.isAnonymous && state !== "global.landing"){
        $message.alert("In order to use this functionality you should login first");
        $state.go("login"); 
      }
      else
      $state.go(state);
  };

  $mocifire.on(['event', 'survey'], function (event) {

    ctrl.survey = event;
  });
  ctrl.go = function (state, params) {
    console.log('ss?');
   if (state == "global.favourites" && ctrl.user.isAnonymous){
      $message.alert("In order to use this functionality you should login first");
      $state.go("login"); 
      
    }
    else {
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
      $state.go(state, params);
    }
  }


  //funciona tanto para como SwipeRight o SwipeLeft
$scope.animacion ={};
$scope.onSwipeRight = function (noti,drag,index){
  if(drag == "right")
    $scope.animacion[index] = {'animated slideOutRight':true};
  else
    $scope.animacion[index] = {'animated slideOutLeft':true};

   setTimeout(function(){
    _firebase.database().ref('pushtoken/'+ctrl.uuid+'/notifications').child(noti.$key).set(null, function(error) {});
    $scope.animacion ={};},500
  )


}

  ctrl.goNotification = function(noti){
  console.log('hioos')
  console.log('params ==>> jdjo ',noti)
  // $ionicHistory.nextViewOptions({
  //   disableBack: false
  // });
  
  //Define que cuando no tenga estado envie al usuario al home
  if(noti.state) { 
   _firebase.database().ref('pushtoken/'+ctrl.uuid+'/notifications').child(noti.$key).child('readed').set(true, function(error) {})
   $state.go(noti.state,{'event_id': noti.eventId, 'chatID':(noti.params)? noti.params.chatID: false, 'pageId':(noti.params)? noti.params.pageId: false})//.then($ionicSideMenuDelegate.toggleRight());
}  else {
    $state.go('main.home', {event_id: noti.eventId}).then($ionicSideMenuDelegate.toggleRight());
    _firebase.database().ref('pushtoken/'+ctrl.uuid+'/notifications').child(noti.$key).child('readed').set(true,function(error) {});
  }
  $ionicSideMenuDelegate.toggleRight()
}

});
