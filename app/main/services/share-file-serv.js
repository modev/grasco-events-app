"use strict";
angular.module("main").service("ShareFile", function($log) {
  var service = this;
  // this is the complete list of currently supported params you can pass to the plugin (all optional)
  service.share = function(file,name) {
    var options = {
      message: "This "+name+" is worth sharing", // not supported on some apps (Facebook, Instagram)
      subject: "This "+name+" is worth sharing", // fi. for email
      files: [file], // an array of filenames either locally or remotely
      chooserTitle: "Pick an app" // Android only, you can override the default share sheet title
    };

    var onSuccess = function(result) {
      console.log("Share completed? " + result.completed); // On Android apps mostly return false even while it's true
      console.log("Shared to app: " + result.app); // On Android result.app is currently empty. On iOS it's empty when sharing is cancelled (result.completed=false)
    };

    var onError = function(msg) {
      console.log("Sharing failed with message: " + msg);
    };

    window.plugins.socialsharing.shareWithOptions(options, onSuccess, onError);
  };

service.openNative = function(file,name){
console.log("abriendo",file);
cordova.InAppBrowser.open(file,"_system","location=yes,enableViewportScale=yes,hidden=yes");

  /*
  cordova.plugins.fileOpener2.showOpenWithDialog(
    '/sdcard/Download/starwars.pdf', // You can also use a Cordova-style file uri: cdvfile://localhost/persistent/Download/starwars.pdf
    'application/pdf', 
    { 
        error : function(e) { 
            console.log('Error status: ' + e.status + ' - Error message: ' + e.message);
        },
        success : function () {
            console.log('file opened successfully'); 				
        }
    }
  );*/
};

service.openNative2 = function(file,name){
  console.log("abriendo",file);
  cordova.InAppBrowser.open(file,"_blank","location=yes,enableViewportScale=yes,hidden=no")
  };

});
