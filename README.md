# WOF APP
WOF APP es una plataforma de eventos para TLS, cuenta con un cms, una aplicación móvil, y un servidor de notificaciones esta adentro del mismo cms.
## Requerimientos
* nodejs v8.9.4
* npm 5.6.0.
* nvm 0.33.8.
* bower cli.
* gulp.

##para generar los resources (splash e icono) se usa este modulo
https://www.npmjs.com/package/ionic-resources
## Entorno de Desarrollo app
https://github.com/mwaylabs/generator-m-ionic
que a su vez esta basado en IONIC 1

## Entorno de Desarrollo cms
Este proyecto web esta desarrollado sobre angular js, el servidor de push notifications esta desarrollado en node

## Proceso de Instalación app
* Clone este repositorio.
* Diríjase al directorio del proyecto y desde el terminal ejecute
```
- rm -rf node_modules
- npm install
- bower i  
Por si no funciona bower i (bower install --allow-root)
- gulp --cordova "prepare"
- gulp
```



* Si todo ha quedado configurado de manera correcta, ya se podría acceder al proyecto desde localhost

