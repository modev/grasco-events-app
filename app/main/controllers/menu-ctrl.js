"use strict";
angular
  .module("main")
  .controller("MenuCtrl", function(
    $state,
    $mocifire,
    $message,
    $user,
    $log,
    menu,
    $ionicHistory,
    event_id,
    _firebase,
    $scope,
    $filter, 
    $ionicSideMenuDelegate,
    $commons
  ) {
    var ctrl = this;
    ctrl.user = $user;
    ctrl.isAnonymous = $user.isAnonymous;
    console.log("ctrl.isAnonymous --->", ctrl.isAnonymous);
    ctrl.menu = menu;
    ctrl.uuid = localStorage.getItem('deviceId')
    
    _firebase.database().ref('pushtoken/'+ctrl.uuid+'/notifications').limitToLast(15).on('value',function(snap){
      // console.log('hola')
      if(!snap.val()) {
        ctrl.badges = 0;
        ctrl.notificaciones = '';
        $commons.apply($scope);
        return

      }
      ctrl.notificaciones = $filter('orderBy')($filter('toArray')(snap.val(), true), 'date', true);
      
      var notificacionesLeidas = $filter("orderBy")(
         $filter("toArray")(snap.val(), true),
         "date",
         true
       ).filter(function(notificacion){ return !notificacion.readed});
       ctrl.badges = (Object.keys(notificacionesLeidas).length== 0)? 0: notificacionesLeidas.length;
    
      console.log('================>>>>>>>>>>>>> ',ctrl.notificaciones)
     $commons.apply($scope);
    })

    ctrl.logout = function() {
      ctrl.user.logout().then(function() {
        $ionicHistory.nextViewOptions({
          disableBack: true
        });
        $state.go("login");
      });
    };

    

    $mocifire.on(["event", "survey"], function(event) {
      ctrl.survey = event;
    });

     ctrl.goHome = function() {
        console.log('que carajos??');
        $state.go('main.home');
        $ionicHistory.clearHistory();
     	};


    ctrl.goProfile = function(state , params) {
      if (state == "global.favourites" && ctrl.user.isAnonymous){
        $message.alert("In order to use this functionality you should login first");
        $state.go("login"); 
        
      }
      else {
        $ionicHistory.nextViewOptions({
          disableBack: true
        });
        $state.go(state, params);
      }
    };


    ctrl.go = function(data) {
      console.log('ede');
      console.log(data.state);
      if (!data.params) data.params = {};
      data.params["event_id"] = event_id;
      if(!data.url){
        if (data.state == "main.favourites" && ctrl.user.isAnonymous) {
          $message.alert(
            "In order to use this functionality you should login first"
          );
          $state.go("login");
        }else if(data.state == 'main.news.list'){ 
          $ionicHistory.nextViewOptions({
            disableBack: false
          });
          $state.go('global.news.list', data.params);
        
        }else {
          $ionicHistory.nextViewOptions({
            disableBack: false
          });
          $state.go(data.state, data.params);
        }
      } else {
        $scope.link(data.url)
      }
    };

    //funciona tanto para como SwipeRight o SwipeLeft
$scope.animacion ={};
$scope.onSwipeRight = function (noti,drag,index){
console.log(noti,drag,index)
  if(drag == "right")
    $scope.animacion[index] = {'animated slideOutRight':true};
  else
    $scope.animacion[index] = {'animated slideOutLeft':true};


   setTimeout(function(){
    _firebase.database().ref('pushtoken/'+ctrl.uuid+'/notifications').child(noti.$key).set(null, function(error) {});
    $scope.animacion ={};},500
  )


}

  ctrl.goNotification = function(noti){
  console.log('hioos')
  console.log('params ==>> jdjo ',noti)
  // $ionicHistory.nextViewOptions({
  //   disableBack: false
  // });
  
  //Define que cuando no tenga estado envie al usuario al home
  if(noti.state) { 
   _firebase.database().ref('pushtoken/'+ctrl.uuid+'/notifications').child(noti.$key).child('readed').set(true, function(error) {})
   $state.go(noti.state,{'chatID':(noti.params)? noti.params.chatID: false, 'pageId':(noti.params)? noti.params.pageId: false})//.then($ionicSideMenuDelegate.toggleRight());
}  else {
    $state.go('main.home').then($ionicSideMenuDelegate.toggleRight());
    _firebase.database().ref('pushtoken/'+ctrl.uuid+'/notifications').child(noti.$key).child('readed').set(true,function(error) {});
  }
  $ionicSideMenuDelegate.toggleRight()
  
}

$scope.link = function(path) {
      cordova.InAppBrowser.open(path, "_system", "location=yes");
    };
  });
