'use strict';
angular.module('exhibitors')
  .controller('exhibitorsCtrl', function ($log, $state, $stateParams, $ionicLoading, $mocifire, $filter, event_id) {

  var ctrl = this;

  var currentPlatform = ionic.Platform.platform();

   //Types of exhibitor
  $mocifire.get('exhibitortypes').then(function (data) {
    ctrl.types = data;
  });
  //End Types of exhibitor

  $mocifire.get(['exhibitors', event_id]).then(function (data) {
    
    angular.forEach(data, function (item,key) {
      data[key].llave = key;
    }); 

    ctrl.exhibitors = $filter('toArray')(data, true);

    angular.forEach(ctrl.exhibitors, function (val) {
      if (val.links && val.links.stores)
        val.links.url = (val.links[currentPlatform]) ? val.links[currentPlatform] : null;
    });

  });

  ctrl.ir = function (id) {
    console.log("id",id);
    $state.go('main.exhibitor', {
      idExhibitor: id
    });
  };

});
