"use strict";
angular
  .module("home")
  .controller("HomeCtrl", function(
    ShareFile,
    $log,
    $scope,
    $mocifire,
    $commons,
    $filter,
    $user,
    $q,
    $CurrentSession,
    $state,
    $ionicNavBarDelegate,
    _logo,
    event_id,
    $ionicHistory,
    $fav,
    $ionicScrollDelegate,
    $message,
    $window,
    menuHome,
    _firebase
  ) {
    var ctrl = this;
    ctrl.user = $user;
    ctrl.ShareFile = ShareFile;
    ctrl.menuHome = menuHome;


    if (!event_id) return $state.go("global.landing");

    /**
     * Scroll init
     */
    var HomeScroll = $ionicScrollDelegate.$getByHandle("HomeScroll");

    /**
     * init
     * @type {*}
     */
    $scope.event_id = event_id;
    $scope.TheTime = {};

    $scope.agenda = {
      loaded: false,
      data: []
    };

    $mocifire.on(["events", event_id, "info"], function(data) {
      if (!data) return;
      $scope.event = data;
      ctrl.msg =
        $scope.event.name +
        " \n Ciudad: " +
        $scope.event.city +
        "\n Lugar: " +
        $scope.event.place +
        " \n" +
        $scope.event.dateFrom;
      console.log(data);
      $commons.apply($scope);
      HomeScroll.resize();
    });

    /*$mocifire.on(['events', event_id, 'content'], function (data) {
         console.info(data);
         if (!data) return;
         $scope.content = data;
         $commons.apply($scope);
         HomeScroll.resize();
         });*/

    $mocifire.on(["events", event_id, "contenido"], function(data) {
      $scope.content = data;
      console.log($scope.content);
    });

    /**
     * Social sharing
     */
    // share anywhere
    /* $scope.share = function () {
            console.log('Sharing...');
            // window.plugins.socialsharing.share($scope.event.name, $scope.event.place, $scope.event.slider[0], $scope.event.tickets, onSuccess, onError);
            window.plugins.socialsharing.share(ctrl.msg, null, 'data:image/png;base64,'+ctrl.imagenData, $scope.event.tickets);
            //share(message, subject, file, url)
        };*/
    $scope.shareFaceBook = function() {
      ctrl.imagenData = ctrl.getBase64Image(document.getElementById("imageid"));
      console.log("Facebook Sharing...");
      window.plugins.socialsharing.share(
        ctrl.msg,
        "data:image/png;base64," + ctrl.imagenData,
        null,
        function() {
          console.log("share ok");
        },
        function(errormsg) {
          console.log(errormsg);
        }
      );
    };

    /**
     * BANNER
     */

    $mocifire.get("adverts/" + $scope.event_id).then(function(data) {
      $scope.adverts = $filter("toArray")(data, true);
      angular.forEach($scope.adverts, function(val) {
        if (val.links && val.links.stores)
          val.links.url = val.links[currentPlatform]
            ? val.links[currentPlatform]
            : null;
      });
    });

    $scope.shareInstagram = function() {
      ctrl.imagenData = ctrl.getBase64Image(document.getElementById("imageid"));
      console.log("Instagram Sharing...");
      // console.log($scope.event.slider[0].url);
      window.plugins.socialsharing.share(
        ctrl.msg,
        "data:image/png;base64," + ctrl.imagenData,
        null,
        function() {
          console.log("share ok");
        },
        function(errormsg) {
          console.log(errormsg);
        }
      );
    };

    ctrl.getBase64Image = function(img) {
      var canvas = document.createElement("canvas");
      canvas.width = img.width;
      canvas.height = img.height;
      var ctx = canvas.getContext("2d");
      ctx.drawImage(img, 0, 0);
      var dataURL = canvas.toDataURL("image/png");
      return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
    };

    /**
     * Abrir link externo
     * @param path
     */
    $scope.link = function(path) {
      cordova.InAppBrowser.open(path, "_system", "location=yes");
    };

    //Ir a faq de evento
    ctrl.goToFaq = function() {
      $state.go("global.faqs", {
        //eventId:ctrl.currentEvent,
        //sessionId:ctrl.currentSession
      });
    };

    /**
     * navegar a otro estado
     * @param state
     * @param disableBack
     */
    $scope.goTo = function(state, disableBack, params) {
      params = params || {};
      $ionicHistory.nextViewOptions({
        disableBack: disableBack || false
      });
      $state.go(state, params);
    };
    $scope.footerGo = function(state) {
      if (ctrl.user.isAnonymous && state !== "global.landing") {
        $message.alert(
          "In order to use this functionality you should login first"
        );
        $state.go("login");
      } else $state.go(state);
    };

    ctrl.go = function(data) {
      console.log('data: ', data);
      if (!data.params) data.params = {};
      data.params["event_id"] = event_id;
      if(!data.url){
        if (data.state == "main.favourites" && ctrl.user.isAnonymous) {
          $message.alert(
            "In order to use this functionality you should login first"
          );
          $state.go("login");
        } else if(data.state == 'main.news.list'){
          $ionicHistory.nextViewOptions({
            disableBack: false
          });
          $state.go('global.news.list', data.params);
        }else{
          $ionicHistory.nextViewOptions({
            disableBack: false
          });
          $state.go(data.state, data.params);
        }
      } else {
        $scope.link(data.url)
      }
    };

    /**
     * Favourites
     * @param check
     */
    $scope.fav = {
      selected: $fav.is("events", event_id)
    };

    $scope.check_fav = function() {
      return $scope.fav.selected;
    };
    $scope.set_fav = function(val) {
      $scope.fav.selected = val;
    };

    $scope.toggle_fav = function(check) {
      if (check) {
        $fav.remove("events", event_id);
        $scope.set_fav(false);
      } else {
        $fav.new("events", event_id);
        $scope.set_fav(true);
      }
    };

    /**
     * contenido dinamico
     */
    var tpls = {
      content: "home/templates/tpls/content.html",
      video: "home/templates/tpls/video.html",
      image: "home/templates/tpls/image.html"
    };

    $scope.get_content_tpl = function(type) {
      return tpls[type];
    };

    $scope.goWithAnonymouscheck = function(state, checkIsAnonymous) {
      if (
        checkIsAnonymous &&
        ctrl.user.isAnonymous &&
        state !== "global.landing"
      ) {
        $message.alert(
          "To enjoy this section you have to be a registered user"
        );
        $state.go("login");
      } else {
        $state.go(state);
      }
    };
  });
