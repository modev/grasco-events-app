'use strict';
var net = angular.module('networking');

net.controller('CardCtrl', function ($log, $scope, $state, $user, $ionicPopup, $cordovaBarcodeScanner, $Contact, $message, $firebaseObject, $q, $ionicHistory) {


	$scope.$on('$ionicView.beforeEnter', function (event, viewData) { viewData.enableBack = true; });
  
	if(!$user.uid){
		$ionicLoading.hide();
		$message.alert("In order to use this functionality you should login first");
		$state.go("login");
	  }

	var ctrl = this;

	ctrl.goHome = function() {
		$state.go('main.agenda');
		$ionicHistory.clearHistory();
	};

	$log.log('user id:', $user.uid);

	$scope.user = {};
	$scope.userid = $user.uid;
	$scope.size = 250;

	$Contact.getUser($scope.userid).then(function (data) {
		$scope.user = data;
	});

	$scope.scan = function() {
		$cordovaBarcodeScanner.scan().then(function(data) {

			$log.log('Dato leido por QR:', data);

			if (!data || data.text == '') return;

			$Contact.acceptRequest($scope.user, data.text).then(function () {
				// success
				$message.alert('User added to your contact list');
			});

		});
	};

	ctrl.showInstruction = function () {
		$ionicPopup.alert({
			title: 'How to add contacts',
			template: '<div class="fw-400 font-small">1. Open the QR code reader. Click on the icon on the top right.<br><br>2. Scan the QR code of another user.<br><br>3. Once the scan is done, the contacts are automatically shared.</div>',
			buttons: [
				{
					text: 'Ok',
					type: 'button-dark'
				}
			]
		});
	};

	ctrl.go = function (state, params) {
	console.log('osf');
    $state.go(state, params);
  }

});
