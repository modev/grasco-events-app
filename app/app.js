'use strict';
angular.module('colombia4', [
  //Bower
  'firebase',
  'angular.filter',
  'leaflet-directive',
  'ja.qr',
  'monospaced.elastic',
  'youtube-embed',
  // app modules
  '_Mocifire',
  'user',
  'favourites',
  'networking',
  'home',
  'agenda',
  'tracks',
  'speakers',
  'exhibitors',
  'surveys',
  'venue',
  'news',
  'livestream',
  'terms',
  'nosotros',
  //'chats',
  'twitter',
  'staticPage',
  'landing',
  'wall',
  'concerts',
  'faqs',
  //Main App
  'push',
  'main'
]);
